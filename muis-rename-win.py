#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""File renamer"""

import requests
import sys
import os
import os.path

# siin peaks olema ka os põhine valik
os.system('cls') #win
#os.system('clear') #osx

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

#dirName = raw_input("Kaust kus asuvad failid: ")
# in python 3
dirName = input("Kaust kus asuvad failid: ")

# see peaks ainult maci puhul olema aktiivne
#dirName = (dirName.replace("\\", "")).rstrip()

if dirName=="":
    input("\n\nKausta nimi puudu!")
    sys.exit()

dirContent = os.listdir(dirName)

i=0

for fileNameIn in dirContent:
    i=i+1
    fileNameArr = fileNameIn.split(".")
    fileExt = fileNameArr[len(fileNameArr)-1]
    fileBase = fileNameArr[0]
    fileBaseArr = fileBase.split("_") #see peaks toetama ka '-' (miinus) märki
    muisid = fileBaseArr[0]
    if len(fileBaseArr)>1:
        seq = fileBaseArr[len(fileBaseArr)-1]
    else:
        seq = ""
    print ("[ " + str(i) + " ]")
    print ("Algfail: " + color.YELLOW + fileNameIn + color.END)
    print ("MuIS ID failist: " + color.YELLOW + muisid + color.END)
    link = "https://www.muis.ee/rdf/object/" + muisid
    r = requests.get(link)

    if (r.status_code!=200):
        print (color.RED + muisid + " pole korrektne MuISi ID" + color.END )
        continue

    import xml.etree.ElementTree as ET
    root = ET.fromstring(r.text.encode('utf-8'))

    for thing in root.findall('{http://www.cidoc-crm.org/cidoc-crm/}E18_Physical_Thing'):
        object = thing.find('{http://purl.org/dc/terms/}identifier')
        objectId = object.text

    print ("Museaali number: " + objectId)

    fileName = objectId.replace(" ", "")
    fileName = fileName.replace(":", "_")
    fileName = fileName.replace("/", "-")

    if seq:
        seq = "_" + seq
    else:
        seq = ""


    fileNameFinal = fileName + seq + '.' + fileExt
    print ("Faili nimi: " + fileNameFinal)


    os.rename(os.path.join(dirName,fileNameIn), os.path.join(dirName,fileNameFinal))

print ("-----------------")
print ("Kokku töödeldi " + color.BOLD + str(i) + color.END + " faili")

input("   ")
